import json
import glob
import shutil
import socket 
import netifaces as ni
import subprocess
import threading
import random
from time import sleep


from flask import Flask
from flask import render_template, redirect, request
from flask_socketio import SocketIO

from evdev import InputDevice, categorize, ecodes
from watchdog.observers import Observer                 # python3-watchdog ?
from watchdog.events import FileSystemEventHandler

import socketio
import requests
http_session = requests.Session()
http_session.verify = False
siocli = socketio.Client(http_session=http_session)

from werkzeug.utils import secure_filename
from zeroconf import Zeroconf, ServiceInfo

import time
import os
import subprocess
import sys
import threading
from subprocess import call
import base64

from src.worker import Worker
from src.logger import get_log
log = get_log('flaks', output="/tmp/tickets.log", log_level="INFO")


#
# CONF
#

# DEFAULT VALUES
config = {
        "name": "Tickets",              # supercharged by hostname
        "model": "unmodified",          # unmodified | modified
        "cutter": "cut",                   # nocut | partcut | cut (or fullcut)
        "font": None,
        "size": '100%',
        "offset": '0%',
        "port": 8050,
        "printpath": "/opt/printer-daemon",
        "mediapath": "/data/usb",
        "debug": False
    }

configpath = "/data/var/tickets.conf"
if len(sys.argv) > 1:
    configpath = sys.argv[1]


def config_set(path, c):
    global config
    config.update(c)
    with open(path, 'w') as cfile:
        json.dump(config, cfile, indent=4)


def config_load(path):
    c = {}
    if os.path.exists(path):
        with open(path) as cfile:
            try:
                c = json.load(cfile)
            except:
                print("Can't load local config.")
    c['name'] = socket.gethostname()
    config_set(path, c)



print("Config File", configpath)

config_load(configpath)

print("Config : {}".format(config))    

isRunning = True

#
# UTILS
#

def get_allip():
    ip = []
    ifaces = ni.interfaces()
    for iface in ifaces:
        if iface.startswith("e") or iface.startswith("w"):
            try:
                i = ni.ifaddresses(iface)[socket.AF_INET][0]['addr']
                if not i.startswith('127'):
                    ip.append(i)
            except:
                pass
    return ip


#
# PRINT CMD
#

def prepare_cmd():
    
    cmd = [os.path.join(os.getcwd(), os.path.join(config['printpath'], 'print')), ]

    # CUT
    if config['cutter'] != 'nocut':
        cmd.append('--cut')
    
    # PARTIAL CUT
    if config['cutter'] == 'partcut':
        cmd.append('--partial')    

    # MODEL
    if config['model'] == "unmodified":
        cmd.append('--old')

    # FONT
    if config['font']:
        cmd.append('--font')
        cmd.append(config['font'])

    # SIZE
    if config['size']:
        cmd.append('--font-size')
        cmd.append(config['size'])
        
    # OFFSET
    if config['offset']:
        cmd.append('--offset')
        cmd.append(config['offset'])
    
    return cmd


def do_print(type='text', data="", pause=0):
    cmd = prepare_cmd()
    
    if (type == 'text'):
        cmd.append('--text')        
    cmd.append(data)
    
    worker.queue.put(cmd)
    
    if pause != 0:
        worker.queue.put(float(pause))


#
# SERVER
#

ALLOWED_EXTENSIONS = ['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif']
UPLOAD_FOLDER = "/tmp"

app = Flask(__name__)
app.config['SECRET_KEY'] = '\xa0\xdf\x8e\xecm3mL>z\xd9j\xd1P\xd9\xab\xcc\xe9%\x0e\xc6\x1a\x02>'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
socketio = SocketIO(app)

worker = None


@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/print', methods=['GET', 'POST'])
def printz():
    
    # PREPARE
    action = request.form.get('action')
    nbr = request.form.get('nbr', 1, int)
    pause = request.form.get('pause', 0, float) / 1000

    # TXT
    if (action == 'text'):
        txt = request.form.get('txt-4')
        if (txt): txt = "\n"+txt
        txt = request.form.get('txt-3')+txt
        if (txt): txt = "\n"+txt
        txt = request.form.get('txt-2')+txt
        if (txt): txt = "\n"+txt
        txt = request.form.get('txt-1')+txt
        
        for i in range(nbr):
            do_print('text', txt, pause)
        
    # FILE
    elif (action == 'file'):
        selfiles = list()
        upload = False
        fileupload = request.files['upload']

        # Direct file upload
        if fileupload.filename != '':
            print("Try to save file: {}".format(fileupload))

            # Save in /tmp
            temppath = os.path.join(UPLOAD_FOLDER, secure_filename(fileupload.filename))
            fileupload.save(temppath)
            selfiles.append( temppath )

            # Save in /data/usb/upload
            try:
                persistent_path = os.path.join(config["mediapath"], "upload")
                if not os.path.exists(persistent_path): os.makedirs(persistent_path)
                call(['cp', temppath, os.path.join(persistent_path, secure_filename(fileupload.filename))])
            except: 
                pass

        # Selected files from list 
        else:
            for f in request.form.get('relpath').split(','):
                selfiles.append( os.path.join(config['mediapath'], f) )

        # Print
        for i in range(nbr):
            for file in selfiles:
                do_print('image', file, pause)
    
    return 'DONE'


# SOCKETIO SERVER
#

@socketio.on('stop')
def stop_printer():
    threading.Thread(target=subprocess.check_call, args=(['/bin/bash', os.path.join(
                           config['printpath'], 'reset.sh')], )).start()

@socketio.on('getlist')
def send_files():
    files_list = []
    find_files(files_list)
    socketio.emit('files', files_list)

@socketio.on('getfonts')
def send_fonts():
    fonts = glob.glob( "/data/fonts/*tf" )
    fonts = [ os.path.basename(f) for f in fonts ]
    fonts.sort()
    socketio.emit('fonts', fonts)
    
@socketio.on('getuplink')
def send_uplink():
    socketio.emit('uplink', siocli.connected)
    
@socketio.on('getconf')
def send_conf():
    socketio.emit('conf', config)
    
@socketio.on('setconf')
def set_conf(c):
    config_set(configpath, c)
    socketio.emit('conf', config)

@socketio.on('clearupload')
def clearupload():
    persistent_path = os.path.join(config["mediapath"], "upload")
    shutil.rmtree(persistent_path)
    send_files()


# SOCKETIO CLIENT
#

@siocli.event
def connect():
    print("Uplink relay connected!")
    siocli.emit('data', {'name': config['name'], 'ip': get_allip(), 'port': config['port'] })
    siocli.emit('join', ['tickets'])
    socketio.emit('uplink', siocli.connected)

@siocli.event
def connect_error(data):
    print("Uplink relay connection failed!", data)
    time.sleep(5)
    siocli.connect('https://relay.kxkm.net/', wait=False)

@siocli.event
def disconnect():
    print("Uplink relay disconnected!")
    socketio.emit('uplink', siocli.connected)
    time.sleep(10)
    siocli_connect()
    
@siocli.on('print')
def on_print(data):
    # print('uplink print', data)
    for txt in data:

        # if data is base64 encoded image, save it to file
        if txt.startswith('data:image') and len( txt.split('base64,') ) > 1 :

            # save base64  image data to file in /tmp with proper extension
            ext = txt.split(';')[0].split('/')[1]
            imgdata = str.encode(txt.split('base64,')[1])

            # filename based on current timestamp
            filepath = os.path.join('/tmp', 'img-'+str(int(time.time()))+'.'+ext)

            # write file
            with open(filepath, "wb") as fh:
                fh.write(base64.decodebytes(imgdata))

            print('saved image to', filepath)
            do_print('image', filepath)
                       
        else:
            fileexist = False

            # if a file exists in mediapath starting with filename, print it
            for file in glob.glob( os.path.join(config['mediapath'], txt)+'.*'):
                do_print('image', file)
                fileexist = True

            # if no file exists, print text            
            if not fileexist:
                do_print('text', txt)


@siocli.on('print-filename')
def on_print_filename(data):
    for filename in data:
        # if a file exists in mediapath starting with filename, print it
        for file in glob.glob( os.path.join(config['mediapath'], filename+'.*') ):
            do_print('image', file)


def siocli_connect():
    print('Connecting to uplink Relay')
    try:
        siocli.connect('https://relay.kxkm.net/', wait=False)
    except:
        print("Uplink relay connecting...")
        pass
    
th = threading.Thread(target=siocli_connect)
th.start()


def find_files(mother_list, relpath=""):
    for file in os.listdir(os.path.join(config['mediapath'], relpath)):
        if file[0] == ".": continue
        
        data = {
            "name": file,
            "relpath": os.path.join(relpath, file)
        }
        if os.path.isdir(os.path.join(config['mediapath'], relpath, file)):
            children = []
            find_files(children, os.path.join(relpath, file))
            data["children"] = children
            mother_list.append(data)
        else:
            if file.split('.')[-1].lower() in  ALLOWED_EXTENSIONS: 
                mother_list.append(data)


@socketio.on('connect')
def connected():
    print('Client connected...')

worker = Worker()
worker.start()


#
# KEYBOARD
#
kb_interface = None
kb_dotHold = False

# Find Keyboard
def kb_detect():
    kbd = subprocess.Popen("ls -la /dev/input/by-id/ | grep event-kbd | awk -F \"/\" '{print $NF}' ", shell=True, stdout=subprocess.PIPE).stdout.read().decode("utf-8").strip()
    return '/dev/input/'+kbd if kbd else None

# Bind to interface
def kb_bind(iface):
    if iface and not isinstance(iface, str):
        iface = iface.src_path
    if iface != kb_detect(): return
    global kb_interface
    try:
        kb_interface = InputDevice(iface)
        kb_interface.grab()
        print("Keyboard connected ...")
    except:
        kb_interface = None
        print("Keyboard not found ...")

def kb_unbind(iface):
    if not isinstance(iface, str):
        iface = iface.src_path
    if iface != kb_detect(): return
    global kb_interface
    kb_interface = None
    print("Keyboard disconnected ...")


# Remote receiver THREAD
def kb_thread():

    print("starting Keyboard listener")
    global kb_interface
    global isRunning
    global kb_dotHold

    while isRunning:
        if not kb_interface:
            sleep(0.5)
            continue

        event = kb_interface.read_one()
        if event and event.type == ecodes.EV_KEY:

            keycode = ecodes.KEY[event.code]
            keymode = ''

            # KEY Event 1
            if event.value == 1:    keymode = 'down'
            elif event.value == 2:  keymode = 'hold'
            elif event.value == 0:  keymode = 'up'
            
            print("keyboard event:", keycode+'-'+keymode)

            # DOT HOLD
            if keycode == 'KEY_KPDOT': kb_dotHold = (keymode != 'up')

            # PRINT #i.*
            if keymode == 'down' and keycode.startswith('KEY_KP'):
                try:
                    keyvalue = int(keycode[6:])
                    if kb_dotHold: keyvalue += 10
                    for file in glob.glob( os.path.join(config['mediapath'], '#'+str(keyvalue)+'.*') ):
                        do_print('image', file)
                except ValueError:
                    pass
                
                # random file #i.0
                if keycode == 'KEY_KPENTER':
                    files = glob.glob( os.path.join(config['mediapath'], '#*.*') )
                    if len(files) > 0:
                        do_print('image', files[random.randint(0, len(files)-1)])

        elif not event:
            sleep(0.03)


    if kb_interface:
        kb_interface.ungrab()

# keyboard connection watchdog
kb_handler = FileSystemEventHandler()
kb_handler.on_created = kb_bind
kb_handler.on_deleted = kb_unbind
kb_observer = Observer()
kb_observer.schedule(kb_handler, '/dev/input/', recursive=False)
kb_observer.start()
kb_interface = None

kb_bind(kb_detect())

kb_threadRun = threading.Thread(target=kb_thread)
kb_threadRun.start()


#
# MAIN
#
if __name__ == '__main__':
    zeroconf = Zeroconf()
    info = ServiceInfo(
        "_http._tcp.local.",
        "tickets._"+config['name']+"._http._tcp.local.",
        addresses=[socket.inet_aton(ip) for ip in get_allip()],
        port=config['port'],
        properties={},
        server=config['name']+".local.",
    )
    zeroconf.register_service(info)

    print("Started on port ", config['port'])
    socketio.run(app, debug=config['debug'], host='0.0.0.0', port=config['port'])

    isRunning = False
    kb_threadRun.join()

    zeroconf.unregister_service(info)
    zeroconf.close()

