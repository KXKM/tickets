#!/bin/bash
BASEPATH="$(dirname "$(readlink -f "$0")")"
pip install -r "$BASEPATH/requirements.txt"

ln -sf "$BASEPATH/tickets@.service" /etc/systemd/system/
ln -sf "$BASEPATH/tickets" /usr/local/bin/

FILE=/boot/starter.txt
if test -f "$FILE"; then
echo "## [tickets] ticket printer [modified|unmodified]
# tickets@modified
" >> /boot/starter.txt
fi