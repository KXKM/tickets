# -*- coding: utf-8 -*-

import logging

_log_level = logging.INFO

_log_levels = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARN": logging.WARN,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR
}

def get_log(module, output=None, log_level=None):
    """
    Return a logger for the module
    """
    log_level = _log_levels[log_level] if log_level is not None else _log_level
    log = logging.getLogger(module)
    if output is None:
        out_hdlr = logging.StreamHandler()
    else:
        out_hdlr = logging.FileHandler(output)
    out_hdlr.setFormatter(logging.Formatter('%(name)s | %(levelname)s |  %(message)s'))
    out_hdlr.setLevel(log_level)
    log.addHandler(out_hdlr)
    log.setLevel(log_level)
    return log