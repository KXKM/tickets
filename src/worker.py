# -*- coding: utf-8 -*-

import time
import threading
import queue
import subprocess

from . import logger
log = logger.get_log('worker', output="/tmp/tickets.log", log_level="DEBUG")


class Worker(threading.Thread):
    def __init__(self):
        super(Worker, self).__init__()
        self.queue = queue.Queue()

    def run(self):
        log.info("Worker started")
        while True:
            event = self.queue.get()
            if event is None:
                log.debug("EXITING..")
                break
            elif type(event) in (int, float):
                log.debug("sleep {}".format(event))
                time.sleep(event)
                continue
            log.debug("Call : {}".format(event))
            try:
                with open("/tmp/worker_printer.log", "a") as fileout:
                    print(event)
                    subprocess.check_call(event, stdout=fileout)
            except Exception as e:
                log.exception("Error in worker : {}".format(e))
            log.debug("--ok call")


