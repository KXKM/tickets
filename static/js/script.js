

if (!Array.prototype.last) {
    Array.prototype.last = function() {
        return this[this.length - 1];
    }; 
};


// SOCKET.IO
//

var socket = io();

socket.on('connect', function(data) {
    console.log('Connected :)');
    $('#servlink').toggleClass('active', true)
    $("#files").empty();
    socket.emit('getlist');
    socket.emit('getuplink');
    socket.emit('getconf');
    socket.emit('getfonts');
});

socket.on('error', console.error.bind(console));
socket.on('message', console.log.bind(console));

socket.on('disconnect', function() {
    console.log('Disconnected :(');
    $('#servlink').toggleClass('active', false)
    $('#uplink').toggleClass('active', false)
    $("#files").empty();
    $("#files").html('No connection to RPi-Tickets..')
});

socket.on('files', (data) => {
    $("#files").empty();
    addDirectory(data, $("#files"));
    $('.collapsible').collapsible();
    sortFiles();
    $("#sort, #sortAlpha").removeClass("hidden");
    $("#sortShuffle").addClass("hidden");

    if (data.length > 0)
        $("<a class='dropdown-trigger btn btn-small' href='#' id='sel-del'>Clear uploads</a>").appendTo("#files").on('click', ()=>{
            if (confirm("Delete all files previously uploaded ?"))
                socket.emit('clearupload')
        })
});

socket.on('fonts', (data) => {
    var dd = $("#font-dd").empty()
    for(let f of data)
        $('<li>').appendTo(dd).append( $('<a>').text(f) )
    
    $('#font-dd a').on('click', (el)=>{
        socket.emit('setconf', {'font': $(el.target).text()})
    })
});

socket.on('conf', (data) => {

    if (data['name']) {
        $('#printerName').html(data['name'].toUpperCase())
        document.title = data['name']
    }
    
    if (data['model']) {
        $('.model').hide()
        $('.model.'+data['model']).show()
        $('#mod-a').html(data['model'])
    }

    if (data['cutter']) {
        $('#cutter-a').html(data['cutter'])
    }

    if (data['font']) {
        $('#font-a').html(data['font'])
    }

    if (data['size']) {
        $('#size-a').html(data['size'])
    }

    if (data['offset']) {
        $('#offset-a').html(data['offset'])
    }
    
});

socket.on('uplink', (data) => {
    $('#uplink').toggleClass('active', data)
});

socket.on('reload', location.reload);


// FILES
//

function sortFiles() {
    $(".collapsible").each(function() {
        collapsible = $(this)
        collapsible.append($(
            collapsible.children(".directory").toArray().sort((a, b) => (a.name > b.name) ? 1 : -1)
        ))
        collapsible.append($(
            collapsible.children(".file").toArray().sort((a, b) => (a.name > b.name) ? 1 : -1)
        ))
    })
}

function shuffleFiles() {
    $(".collapsible").each(function() {
        collapsible = $(this)
        collapsible.append($(
            shuffle(collapsible.children(".directory").toArray())
        ))
        collapsible.append($(
            shuffle(collapsible.children(".file").toArray())
        ))
    })
}

function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function toggle_childrens(directory) {
    //	    console.log(directory);
    directory.parentElement.parentElement.parentElement.querySelectorAll('input.filebox').forEach((elem) => {
        elem.checked = directory.checked;
    });
}

function addDirectory(list, container) {
    var ul = $('<ul class="collapsible">').appendTo(container)

    for (var el of list) {

        // directory
        if (el.children) {
            if (el.children.length) {
                var li = $('<li>').appendTo(ul)
                var hd = $('<div class="collapsible-header">').append('<i class="material-icons">folder_open</i>')
                    .append('<label><input class="" type="checkbox" onclick="toggle_childrens(this);" /><span></span></label>')
                    .append(el.name).appendTo(li)
                var bd = $('<div class="collapsible-body">').appendTo(li)
                addDirectory(el.children, bd)
                li.addClass('directory')
                li.prop("name", el.name)
            }
        }
        // file
        else {
            var li = $('<li>').appendTo(ul)
            var hd = $('<div class="collapsible-header">')
                // .append('<i class="material-icons">image</i>')
                .append('<label><input class="filebox subfile" type="checkbox" value="' + el.relpath + '" /><span></span></label>')
                .append(el.name).appendTo(li)
            li.addClass('file')
            li.prop("name", el.name)
        }
    }
}


//
// ACTIONS
//

// AJAX FORM
$("form#printform").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);
    $.ajax({
        url: $(this).attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
            console.log(data)
        },
        error: function (data) {
            console.log(data)
        },
        cache: false,
        contentType: false,
        processData: false
    });
    $('#printaction').val('')
});

// PRINT
//
$('#doPrint').on('click', () => 
{
    // Print TXT
    if ($('#txt-1').val()+$('#txt-2').val()+$('#txt-3').val()+$('#txt-4').val() != "") {
        $('#printaction').val('text')
        $('#print_relpath').val('')
        $('#filebtn').val('')
        $('#filelist').val('')
        $('#printform').submit()
    } 
    // Print UPLOAD FILE
    else if ($('#filelist').val() != "") {
        $('#printaction').val('file')
        $('#print_relpath').val('')
        $('#printform').submit()
    }
    // Print SELECTED FILES
    else {
        var selectedFiles = []
        $('.filebox:checkbox:checked').each((i, el) => {
            // console.log($(el).val())
            selectedFiles.push($(el).val())
        })
        if (selectedFiles.length == 0) return 
        $('#printaction').val('file')
        $('#print_relpath').val(selectedFiles.join(','))
        $('#printform').submit()
    }
})

// STOP
//
$('#stop').on('click', () => { socket.emit('stop') })

// STOP
//
$('#clear').on('click', () => { 
    $('#txt-1').val('')
    $('#txt-2').val('')
    $('#txt-3').val('')
    $('#txt-4').val('')
    $('#print_relpath').val('')
    $('#filebtn').val('')
    $('#filelist').val('')
 })

// SORT
//
$('#sort').on('click', () => {
    $('#sortAlpha, #sortShuffle').toggleClass("hidden");

    if ($('#sortAlpha').hasClass("hidden")) {
        shuffleFiles()
    } else {
        sortFiles()
    }
});


// TXT
//
// $(".txt-line").hide()
$(".txt-line1").on('click', ()=>{
    $(".txt-line").show()
})


//
// CONF
//

$('.dropdown-trigger').dropdown();

// MODEL
$('#mod-dd a').on('click', (el)=>{
    socket.emit('setconf', {'model': $(el.target).text()})
})

// CUTTER
$('#cutter-dd a').on('click', (el)=>{
    socket.emit('setconf', {'cutter': $(el.target).text()})
})

// SIZE
$('#size-dd a').on('click', (el)=>{
    let txt = $(el.target).text()
    if (txt.startsWith('SIZE')) txt = txt.substring(5)
    socket.emit('setconf', {'size': txt})
    console.log(txt)
})

// OFFSET
$('#offset-dd a').on('click', (el)=>{
    let txt = $(el.target).text()
    if (txt.startsWith('OFFSET')) txt = txt.substring(7)
    socket.emit('setconf', {'offset': txt})
    console.log(txt)
})
